package com.example.wordsfactory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.wordsfactory.databinding.ActivityOnboardingBinding

class OnboardingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOnboardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.skipButton.setOnClickListener {
            startActivity(
                Intent(this, SignUpActivity::class.java)
            )

            finish()
        }

        binding.nextButton.setOnClickListener {
            if (binding.onboardingList.currentItem != binding.onboardingList.adapter!!.itemCount - 1)
                binding.onboardingList.currentItem += 1
            else {
                startActivity(
                    Intent(this, SignUpActivity::class.java)
                )

                finish()
            }
        }

        binding.onboardingList.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.onboardingList.adapter =
            OnboardingAdapter(this, Constance.onboardingPagesList)
    }
}
package com.example.wordsfactory

data class MeaningData(
    val meaning: String,
    val example: String

)

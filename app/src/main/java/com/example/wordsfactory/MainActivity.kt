package com.example.wordsfactory

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wordsfactory.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            if (item.itemId == R.id.bottomDictionary)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragments, DictionaryFragment())
                    .commit()

            else if (item.itemId == R.id.bottomVideo)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragments, VideoFragment())
                    .commit()

            true
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragments, DictionaryFragment())
            .commit()


    }
}
package com.example.wordsfactory

object Constance {
    val onboardingPagesList = listOf(
        OnboardingData(
            R.drawable.onboarding_image_1,
            "Learn anytime and anywhere",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.tochki_1
        ),OnboardingData(
            R.drawable.onboarding_image_2,
            "Find a course for you",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.tochki_2
        ),OnboardingData(
            R.drawable.onboarding_image_3,
            "Improve your skills",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.tochki_3
        )
    )
    val meaningList = listOf(

        MeaningData("The practice or skill of preparing food by combining, mixing, and heating ingredients.","Example: he developed an interest in cooking."),
        MeaningData("The practice or skill of preparing food by combining, mixing, and heating ingredients.","Example: he developed an interest in cooking.")
    )
}
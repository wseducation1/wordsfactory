package com.example.wordsfactory

data class OnboardingData(
    val image: Int,
    val title: String,
    val subtitle: String,
    val tochki: Int

)

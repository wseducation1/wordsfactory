package com.example.wordsfactory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wordsfactory.databinding.ActivityOnboardingBinding
import com.example.wordsfactory.databinding.OnboardingItemBinding

class OnboardingAdapter(
    private val context: Context,
    private val onboardingPages: List<OnboardingData>
): RecyclerView.Adapter<OnboardingAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.onboarding_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = OnboardingItemBinding.bind(holder.itemView)
        val pages = onboardingPages[position]

        binding.onboardingImage.setImageResource(pages.image)
        binding.onboardingTitle.text = pages.title
        binding.onboardingSubtitle.text = pages.subtitle
        binding.tochkiImage.setImageResource(pages.tochki)

    }

    override fun getItemCount() = onboardingPages.size

}
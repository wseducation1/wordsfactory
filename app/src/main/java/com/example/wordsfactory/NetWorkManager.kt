package com.example.wordsfactory

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path

interface NetWorkManager {
    companion object {
        val instance = Retrofit.Builder()
            .baseUrl("https://api.dictionaryapi.dev/api/v2/entries/en/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NetWorkManager::class.java)

    }

    @GET("{word}")
    fun getWordInfo(@Path("word") word: String): Call<List<WordInfo>>
}
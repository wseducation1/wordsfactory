package com.example.wordsfactory

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wordsfactory.Constance.meaningList
import com.example.wordsfactory.databinding.FragmentDictionaryBinding

class DictionaryFragment(

): Fragment(R.layout.fragment_dictionary) {

    private lateinit var binding: FragmentDictionaryBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentDictionaryBinding.bind(view)

        binding.searchImage.setOnClickListener {
            binding.noWord.visibility = View.GONE
            binding.aboutWord.visibility = View.VISIBLE
        }





        binding.meaningsExample.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.meaningsExample.adapter = MeaningAdapter(requireContext(), meaningList)
    }
}
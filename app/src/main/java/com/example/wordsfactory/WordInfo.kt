package com.example.wordsfactory

data class WordInfo(

    val word: String,
    val phonetic: String,

)

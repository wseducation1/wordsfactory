package com.example.wordsfactory

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.wordsfactory.databinding.VideoItemBinding

class VideoFragment : Fragment(R.layout.video_item) {
    private lateinit var binding: VideoItemBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = VideoItemBinding.bind(view)

        binding.webView.loadUrl("https://google.com")
    }
}
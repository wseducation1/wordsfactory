package com.example.wordsfactory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wordsfactory.databinding.DictionaryMeaningItemBinding

class MeaningAdapter(
    private val context: Context,
    private val meaning: List<MeaningData>
): RecyclerView.Adapter<MeaningAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.dictionary_meaning_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = DictionaryMeaningItemBinding.bind(holder.itemView)
        val currentMeanings = meaning[position]

        binding.meaningText.text = currentMeanings.meaning
        binding.exampleText.text = currentMeanings.example



    }

    override fun getItemCount() = meaning.size
}